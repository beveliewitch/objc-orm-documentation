
1. [Introduction](#introduction)
2. [Créer un mapping avec un fichier XML](#créer-un-mapping-avec-un-fichier-xml)    
    1. [Où placer le fichier de mapping ?](#ou-placer-le-fichier-de-mapping-)
    2. [Ecrire le mapping](#ecrire-le-mapping)
        1. [Minimum requis](#minimum-requis)
        2. [Décrire un héritage](#décrire-un-héritage)
        3. [Décrire un champ](#décrire-un-champ)
        4. [Décrire une relation](#décrire-une-relation)
3. [Créer un mapping avec les annotations de classes](#créer-un-mapping-avec-les-annotations-de-classes)
    1. [Ecrire les annotations](#ecrire-les-annotations)
        1. [Décrire un héritage](#décrire-un-héritage-1)
        2. [Décrire un champ](#décrire-un-champ-1)
        3. [Décrire une relation](#décrire-une-relation-1)
    2. [Générer le mapping depuis les annotations](#générer-le-mapping-depuis-les-annotations)


# Introduction

Le mapping sert à faire la correspondance entre le modèle conceptuel de donnée (UML) et le modèle physique de données (ici une base de données MySQL). Ce mapping peut être décrit :

* Au format xml : un fichier global pour l’ensemble des entités.
* Sous forme d’annotations (des macros) dans chacune des classes qui permettront de générer le fichier xml.

Une entité est une structure abstraite qui défini le lien entre une classe et une table.

# Créer un mapping avec un fichier XML

## Ou placer le fichier de mapping ?

Où placer + configuration du projet pour copier au bon endroit… bundle truc

## Ecrire le mapping

### Minimum requis

Une classe mère unique, dans notre cas il s’agit de la classe `eObjet` mappée sur l’entité nommée `objet`.

```xml
<objet class=”eObjet”>
    <autoincrement></autoincrement>
</objet>
```

Le plus important ai qu’il y est une entité mère unique **dont dérivent directement ou indirectement l’ensemble des autres entités**. Dans l’entité mère doit être défini la balise `autoincrement`, c’est la seule entité à devoir faire ça.

### Décrire une entité

```xml
<entity class=”Entity”></entity>
```

Ce qui permet de décrire que l'entité nommée "entity" est attachée à la classe "Entity".

### Décrire un héritage

```xml
<superEntity class=”SuperEntity”></superEntity>

<entity class=”Entity”>
    <extends>superEntity</extends>
</entity>

```

La balise `extends` permet de décrire que l'entité `entity` attachée à la classe `Entity` hérite de l'entité `superEntity`.

On indique que l’entité entity étends l’entité superEntity. Notez bien qu’une entité étends une autre entité et non une classe. **Cela implique et nécessite que la classe Entity étende la classe SuperEntity au niveau du code source des classes. Cette règle doit être respectée sans quoi l’ORM provoquera des erreurs.**

### Décrire un champ

```xml
<entity class=”Entity”>
    <field name=”myField” db-name=”myField” type=”string” />
</entity>
```

* `field`: indique qu’il s’agit d’un champs simple
* `name`: indique le nom du champ / propriété dans la classe “db-name” : indique le nom de la colonne dans la table 
* `type`: indique le type du champ dans la classe

Les types scalaires autorisés sont les suivants

* `string` : chaine de caractères
* `int`/`integer` : entier
* `bool`/`boolean` : booléen

On indique que la classe Entity à un champ myField de type string que l’on fait correspondre à une colonne de nom myField dans la table entity.

### Décrire une relation

```xml
<entity class=”Entity”>
    <one name=”myRelation” db-name=”myRelation” entity=”otherEntity”  /> 
</entity>
```

Les relations autorisées sont les suivantes

* `one`: on pointe vers un seul objet (relation 1-1 ou n-1)
* `many`: on pointe vers une collection d’objet (relation 1-n ou n-n)

* `name`: indique le nom du champ / propriété dans la classe 
* `db-name`: indique le nom de la colonne dans la table 
* `entity`: le type de l’entité vers laquelle pointe la relation
 
**Si la relation est définie dans les deux sens**, on obtient une relation **bi-directionnelle** sinon elle est **uni-directionnelle**.

Par exemple, on défini ci-dessous une **relation bi-directionnelle** de type `one-many` entre `entity` et `otherEntity` :

```xml
<entity class=”Entity”>
    <one name=”myRelation” db-name=”myRelation” entity=”otherEntity”  />
</entity>

<otherEntity class=”OtherEntity”>
    <many name=”myRelationInverse” db-name=”myRelationInverse” entity=”entity”  /> 
</otherEntity>
```
#### Comportement des relations

On peut associer deux comportements aux relations décrites, la **cascade** et la **composition**.

##### Cascade

Si on ajoute la composante “cascade” avec comme valeur “true”, on considère que la relation est em cascade. Cela signifie que lorsque l’on supprime un objet de la classe associée à l’entité contenant la relation, le/ou les objets pointée(s) par cette relation seront eux aussi supprimé(s).

```xml
<entity class=”Entity”>
    <one name=”myRelation” db-name=”myRelation” entity=”otherEntity” cascade=”true” /> 
</entity>
```

Par exemple, ci-dessus, lorsque l’on supprime un objet de type Entity, l’objet reférencé par myRelation sera aussi supprimé.

##### Composition

Si on ajoute la composante **composition** avec comme valeur `true`, on considère que la relation est une composition. Cela signifie que si on affecte un nouvel objet à la relation, **l’ancien qui y était associé sera supprimé car il ne peut exister seul. La composition contient le comportement de la cascade.**

```xml
<entity class=”Entity”>
    <one name=”myRelation” db-name=”myRelation” entity=”otherEntity” composition=”true” />
</entity>
```
## Synthétiser les propriétés et les relations pour l'ORM

Habituellement la synthétisation des propriétés s'effectue via la syntaxe `@synthesize nomPropriété`. Dans le cas de l'ORM l'utilisation de macros particulières est nécessaire afin d'effectuer la liaison et permettre l'exécution des opérations de traitements de la base de données.

 * `synthesizedb(nomPropriété, setNomPropriété, typePointeur*)` : Pour les objets
 * `synthesizedb_int(nomPropriété, setNomPropriété)` : pour les entiers primitifs (`int`)
 * `synthesizedb_float(nomPropriété, setNomPropriété)` : pour les flottant à simple précision primitifs (`float`)
 * `synthesizedb_bool(nomPropriété, setNomPropriété)` : pour les booléen primitifs (`BOOL`)

Avec

 * nomPropriété : le nom de la propriété à synthétiser
 * setNomPropriété : le nom du setter de la propriété à synthétiser
 * typePointeur* : Un type pointeur, par exemple `NSString*` ou encore `MonObjet*`

# Créer un mapping avec les annotations de classes

Note : le mapping par annotations de classe est une sur-couche pour **eMaket** qui se trouve dans le framework `eObjet`. Cette méthode est obsolète, préferez l'annotation XML.

Vous pouvez choisir de représenter le mapping par un fichier xml (comme vu précédement) ou bien par un ensemble d’annotations à écrire dans les classes. Ces annotations permettrons de générer le mapping XML adéquat. Les annotations de classes sont juste une manière différente de décrire la structure et les relations entre les objets. Il sert donc à la même chose que le fichier XML.

## Ecrire les annotations

### Décrire une entité

```obj-c

@implementation Entity

map_objet(entity);

@end
```

Ce qui permet de décrire que l'entité `entity` est attachée à la classe `Entity`.

### Décrire un héritage

L'héritage n'as pas besoin d'être décrit, il est directement déduit de la classe.

### Décrire un champ

Pour décrire un champ il faut utiliser les macros qui synthétisent la propriété pour la base de données. La synthétisation est de forme :

```obj-c
synthesize[_diffname]_<type>(nomPropriété[, nomColonne], setNomPropriété);
creer_colonne_<type2>(nom);
TODO
```

Avec 
 * `diffname` optionnel : permet de mapper un champ à une colonne de la table de nom différent
 * `nomColonne` : requis uniquement si diffname est défini
 * `<type>` qui peut-être : chaine, entier16, entier, booleen, float, reel, simple_tab
 * `<type2>` qui peut-être : chaine, entier, reel, booleen, simple_tableau

Sachant que le `<type>` et le `<type2>` doivent être cohérents.

#### Exemples

##### Chaine de caractères

```obj-c
synthesize_diffname_chaine(nomPropriété, nomPropriétéBDD, setNomPropriété);
creer_colonne_chaine(nomPropriétéBDD);
```

##### Entier 16

```obj-c
synthesize_diffname_entier16(typeObjet, typeobjet, setTypeObjet);
```

##### Booléen avec nom identique sur la colonne de la table

```obj-c
synthesize_booleen(marque, setMarque);
```

### Décrire une relation

Pour décrire une relation entre entités, il faut utiliser la macro qui synthétise la relation pour la base de données. Elle est de forme :

```obj-c
synthesize[_diffname]_<genreManagé>[_destructionReaffectation](nomPropriété[, nomColonne], setNomPropriété, nomType);
```

Avec 
 * `diffname` optionnel : permet de mapper un champ à une colonne de la table de noms différents
 * `nomColonne` : requis uniquement si diffname est défini
 * `<genreManagé>` qui peut-être : objet, tableau
 * `destructionReaffectation` optionnel : signifie que lors de l'affectation d'un nouvel objet / tableau à cette relation, l'ancienne sera supprimé de la base de données, il s'agit d'une composition (à l'opposition de l'aggrégation).

#### Exemples

##### Objet en destruction lors d'une réaffectation

```obj-c
synthesize_diffname_objet_destructionReaffectation(imagette, imagettefichier, setImagette, nFichier*);
```

Ici, l'affectation d'une nouvelle imagette provoquera la suppression de l'ancienne en base de données.

##### Tableau en destruction lors d'une réaffectation

```obj-c
synthesize_tableau_destructionReaffectation(personne_estvisible_eobjet, setPersonne_estvisible_eobjet, nPersonne_visibilite_eObjet*);
```

Ici, le remplacement d'un objet dans le tableau provoquera la suppression de l'ancience en base de données. (TODO vérifier)

## Générer le mapping depuis les annotations

Lorsque vous utilisez le système des annotations, il est nécessaire de générer le fichier de mapping XML depuis celles-ci. Il existe une méthode permettant d'effectuer ce travail.

```obj-c    
[Mapping generateXML:@"mapping.xml"];
```

Le plus courant est d'effectuer ce traitement au démarrage de l'application dans le selecteur :

```obj-c   
- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
```
