1. [Qu'est ce que la couche abstraite d'accès aux données](#quest-ce-que-la-couche-abstraite-daccès-aux-données)
2. [Configuration de la connection](#configuration-de-la-connection)
3. [Initialisation de la DBAL](#initialisation-de-la-dbal)
4. [Générer la base de données à partir du mapping](#générer-la-base-de-données-à-partir-du-mapping)
5. [Contexte de la DBAL](#contexte-de-la-dbal)


# Qu'est ce que la couche abstraite d'accès aux données

La couche abstraite d'accès aux données permet :

 * D'effectuer et de générer les requêtes pour toutes les opérations de traitement de base de données : insertion, création, modification, suppression
 * De faire le mapping entre les résultats bruts en provenance de la base de données vers les objets
 * Une persistance des objets et de leurs relations en mémoire via un contexte

Cette couche abstraite d'accès aux données se nomme **DBAL** pour DataBase Abstract Layer.

# Configuration de la connection

Plusieurs instance de DBAL peuvent cohabiter au sein d'une même application. Il est donc possible de l'instancier avec différents paramètres. Cependant pour définir une instance principale, il est possible de configurer la connexion via un fichier `configuration.strings` de la manière suivante :

TODO

# Initialisation de la DBAL

Pour initialiser un objet DBAL basé sur la configuration, il suffit de faire appel à `[defaultDBAL]`

```obj-c
DBAL *dbal = [DBAL defaultDBAL];
```

**Surcouche eMaket**

```obj-c
BDD *bdd = TODO
```

# Générer la base de données à partir du mapping

Une fois le mapping défini, il est possible de générer la base de données correspondante a celui-ci. Le fichier XML du mapping doit exister, ce qui signifie que soit vous avez choisi l'annotation XML, soit vous avez préalablement généré le fichier XML depuis les annotations de classes.

```obj-c
[dbal generateDb];
```

**Surcouche eMaket**

```obj-c
[bdd genererBDD]
```

# Contexte de la DBAL

